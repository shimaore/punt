
/**
 * Module dependencies.
 */

var Emitter = require('events').EventEmitter;
var Message = require('@shimaore/amp-message');
var dgram = require('dgram');
var url = require('url');

/**
 * Expose `Server`.
 */

module.exports = Server;

/**
 * Initialize a `Server` with the given `addr`.
 *
 * @param {String} addr
 * @api public
 */

function Server(addr) {
  if (!(this instanceof Server)) return new Server(addr).bind();
  this.addr = url.parse(addr);
  this.onmessage = this.onmessage.bind(this);
  this.sock = dgram.createSocket(this.addr.protocol.replace(/:$/,''));
  this.info = null;
}

/**
 * Inherit from `Emitter.prototype`.
 */

Server.prototype.__proto__ = Emitter.prototype;

/**
 * Bind.
 *
 * @api private
 */

Server.prototype.bind = function(){
  var self = this;
  var sock = this.sock;
  sock.bind(this.addr.port, this.addr.hostname);
  sock.on('message', this.onmessage);
  sock.on('listening', function(){
    self.address = sock.address();
    self.emit('bind');
  });
  return this;
};

/**
 * Handle messages.
 *
 * @api private
 */

Server.prototype.onmessage = function(buf, rinfo){
  var msg;
  try {
    msg = new Message(buf);
  } catch (err) {
    this.emit('parse-error', err, buf);
    return;
  }
  this.info = rinfo;
  this.emit('message',...msg.args);
};
